import math

true = 1
false = 0
x1 = [] # List of factors 

def lcm(x, y):
	   if x > y:
	       z = x
	   else:
	       z = y
	
	   while(True):
	       if((z % x == 0) and (z % y == 0)):
	           lcm = z
	           break
	       z += 1
	
	   return lcm

def gcf(a, b):
    """Calculate the Greatest Common Divisor of a and b.

    Unless b==0, the result will have the same sign as b (so that when
    b is divided by it, the result comes out positive).
    """
    while b:
        a, b = b, a%b
    return a

def prime_factors(n):
    result = []
    for i in range(2,n):
        while n % i == 0:
            #print i,"|",n
            n = n/i
            result.append(i)

        if n == 1: 
            break

    if n > 1: result.append(n)
  
    return result

# Main program - Supports command line arguments if needed "
if __name__ == "__main__":

    # Call factor tree
    print("Getting Factor Tree, Factors , lcm and gcf of two numbers\n")
    keepRunning = true
    
    while(keepRunning == true):
        x = input("Enter first number: ")
        y = input("Enter second number: ")
        print("\n")
        x = int(x)
        y = int(y)
        print("The prime factors of: ",x,prime_factors(x))
        print("The prime factors of: ",y,prime_factors(y)) 
        print("The lcm is:" ,lcm(x,y))
        print("The gcf is:" ,gcf(x,y))
        runAgain = input("Run again? type 'y' or 'n'  ")
        if(runAgain == 'y'):
            keepRunning = true
        elif(runAgain == 'n'):
            keepRunning = false
            




"""
    # DEPRECATED BELOW
def factors(x):
    # factoring 'x' values range value is the fixed numerator
    for i in range(1,(x+1)): # This range expression takes care of initialize to 1 and ending at specified numerator
        
        # check to see if a remainder occurs first else run division
        mod = x%i
        
        if mod > 0:
            #zero out modulus for next iteration
            mod = 0
        else:
            temp = x/i # number divided and added to list

            #print(temp)
            # fill in x1 list
            x1.append(temp)


    #Is this just a prime number     
    if len(x1) == 1 or len(x1) == 2:
        print("This is a prime number")

    # Getting the factor tree
    return prime_factors(x)

def divideDown(x):
    

    # factoring 'x' values range value is the fixed numerator
    #for i in range(1,(x+1)): # This range expression takes care of initialize to 1 and ending at specified numerator
    xin = x
    i = 2
    divDown = true
    while divDown == true:  
        
        # check to see if a remainder occurs first else run division
        mod = x%i
        
        if mod > 0:
            #zero out modulus for next iteration
            mod = 0
        else:
            temp = x/i # number divided and added to list
            #print(temp)
            
            if i < temp:
                # can the new this be divided down further
                print ("Square root of i ->",i ,math.sqrt(i))
                if (math.sqrt(i).is_integer()):
                    print ("Square root of ias integer ->",i ,math.sqrt(i))
                    xtree.append(math.sqrt(i))
                    xtree.append(math.sqrt(i))
                else:
                    xtree.append(i)
                x=temp
                # can the new temp be divided down further
                
            else:
                divDown = false
                xtree.append(i)
        i=i+1
        #x=temp
        if i > xin:
           divDown = false 
        print("This is i",i)
                
    print(xtree)        
            
"""
