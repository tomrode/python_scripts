#!/usr/bin/python

import smtplib

sender = 'Tom.Rode@Crown.com'
receivers = ['Tom.Rode@Crown.com']

message = """From: From Person <Tom.Rode@Crown.com>
To: To Person <Tom.Rode@Crown.com>
Subject: SMTP e-mail test

This is a test e-mail message.
"""

try:
   smtpObj = smtplib.SMTP('localhost')
   smtpObj.sendmail(sender, receivers, message)         
   print ("Successfully sent email")
except SMTPException:
   print ("Error: unable to send email")
