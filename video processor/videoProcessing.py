################################################################################
# File name:  videoProcessing.py
#
# Description: The script will use specified video in this case .mp4 format then:
#              1. display original video
#              2. resizing (640 x 480)
#              3. run a 3x3 matrix for sharpening
#              4. saves a new video file as a result 
#
# Date:  7/15/2020
# Author: Tom Rode
#################################################################################

import sys
import cv2 
import numpy as np

LIGHT_LEVEL = .8   # The higher this number the darker .1 - 10
  
def videoprocess(argv):
    # Creating a VideoCapture object to read the video 
    #cap = cv2.VideoCapture('DJI_0021.MP4')
    #cap = cv2.VideoCapture('blur6.mp4')
    
    # Call video processing
    cap = cv2.VideoCapture(sys.argv[1])
   
        
    # set up to save output
    out = cv2.VideoWriter('Modified output.mp4', -1, 20.0, (640,480))

    try:
        # Loop untill the end of the video 
        while (cap.isOpened()): 
            # Capture frame-by-frame 
            ret, frame = cap.read() 
            #frame = cv2.resize(frame, (640, 480), fx = 30, fy = 50,
            frame = cv2.resize(frame, (640, 480), fx = 50, fy = 50, 
                                 interpolation = cv2.INTER_CUBIC) 
          
            # Display the resulting frame 
            cv2.imshow('Original', frame)  
         
            #kernel = np.array([[-1, -1, -1],[-1, 9, -1],[-1, -1, 0]], np.float32)

            # 3x3 matrix, Convolves an image with the kernel These values are pretty optimal.
            kernel = np.array([[0, -1, 0],[-1, 5, -1],[0, -1, 0]], np.float32)
            #kernel = np.array([[-1, -1, -1],[-1, 5, -1],[-1, -1, 0]], np.float32)
            kernel = 1/LIGHT_LEVEL * kernel # the fraction changes the light level
            im = cv2.filter2D(frame, -1, kernel)
           
            # output this to screen
            cv2.imshow('Modified', im)

            # write the altered frames to a file 
            out.write(im)
          
            # define q as the exit button 
            if cv2.waitKey(25) & 0xFF == ord('q'):
                print('q hit\n')
                break

    except:
            
        print('Video complete with exception\n')
        # release the video capture object 
        cap.release() 
                  
        # Closes all the windows currently opened.
        cv2.destroyAllWindows()
        
    print('Video complete\n')  

# Main program - Supports command line arguments if needed "
if __name__ == "__main__":

      videoprocess(sys.argv)
    
        


