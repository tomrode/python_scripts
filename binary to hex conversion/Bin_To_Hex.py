# Name:      Bin_To_Hex.py
#
# Purpose:   This python script will convert a binary string to a hexidecimal string.
#            It will open a bimary file, perform a conversion then write a hexidecimal file   
#         
# Author:    Thomas Rode
#
# Options:   None
#
# Note:
#
# Python Version: 3.7.4
#          
# Reference: https://www.python.org/ (open source)
#
import binascii


# Global variables
binFileName = "Bin.txt"
hexFileName = "Hex.txt"
defaultStr = "Hi there, here is some binary string%^&*)(*^%%#$%Yebhboadfnvakdnv*&^%%$#$^&*(*&^%$"

def bin2hex():
    

    # Open the binary file, capture data
    try:
        f = open(binFileName, "r")
        print("File contents ->", f.read(), "  Input File Name ->",binFileName )
        print("\n")
        binStr = f
        f.close()
    except:
        print("Missing binary text file input, defaulting to internal string")
        binStr = defaultStr


    # Make the conversion 
    try:
        # convert raw string to bytes
        bytes_str = bytes(defaultStr, 'utf-8')
        hex_str = binascii.hexlify(bytes_str)

        # convert so it writes file
        hex_str = str(hex_str, 'utf-8')
    except:
        print("Error in conversion")
        

    # Save conversion and output
    try:
        f = open(hexFileName, 'w')
        f.write(hex_str)
        f.close()
        print("Hex conversion result ->",hex_str, "  Output File Name ->",hexFileName )
    except:
         print("Error in file save")
         
    
    
# Main program - Supports command line arguments if needed "
if __name__ == "__main__":

    # Call conversion
    bin2hex()
